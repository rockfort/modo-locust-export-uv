Export UVs with Bounds
======================

## Summary

  When you create Textures with Adobe Illustrator,
  your exported UV EPS will have no bounds. 
  
  This script will add a temporary plane to your scene for the bounds and delete it again after export.

## Intallation

  1. [Download the Script](https://bitbucket.org/rockfort/modo-locust-export-uv/get/master.zip)

  2. Drop the zips folder contents into your scripts directory into a new folder called "locust_uv_export"

## Useage

    @locust_uv_export/scripts/locust_export_uv.py