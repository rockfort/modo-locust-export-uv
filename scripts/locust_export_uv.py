#python

##============================================================================##
# Export Texture UV with Bounds
##----------------------------------------------------------------------------##
# Description:
# Export UV Sets with Bounds, so you can 
# adjust the illustrator canvas to the UV Bounds.
##============================================================================##

#==============================================================================#
# Imports                                                                       
#==============================================================================#

import traceback
from sys import exit
from lx import eval as lxe, evalN as lxN, eval1 as lx1, out as lxo
import time

#==============================================================================#
# Functions
#==============================================================================#

# [create_plane]
# Creates a plane with a give name
# Adds a date & time string
# @1: Name
def create_plane(name):

  d = time.strftime("%Y%m%d")
  t = time.strftime("%H%M%S")

  n = name + d + t

  lx.eval('layer.new')
  lx.eval('@MakePlane.pl')
  lx.eval('item.name {%s}' % n)

  return n

# [select_all_items]:
# Select all layers in the current scene of a type
# @1: Type (Mesh, Instance, Etc)
# $1: an array of the selected items
def select_all_items(type):

    # Get all the items of the requested type
    items = return_scene_items(type)

    # Select all the items
    for item in items: lxe('select.item {%s} add' % item)
    
    # Return the array of all the items
    return items

# [return_scene_items]:
# @1: 'mesh':  Mesh List
# @1: 'item':  Item List
# @1: 'scene': Scene List
# $1all items from the requested type in the scene
def return_scene_items(type):

    item_list = []

    item_count = lxe('query sceneservice %s.N ?' % type)

    # Append all items to the list
    # Scenes have an index instead of an ID
    if type == 'scene':
        for n in range(item_count):
            item_list.append(lxe('query sceneservice scene.index ? {%s}' % n))
    elif type == 'mesh' or type == 'meshInst' or type == 'item':
        for n in range(item_count):
            item_list.append(lxe('query sceneservice %s.id ? {%s}' % (type, n)))

    return item_list


#==============================================================================#
# Main
#==============================================================================#

def main():

  # Create a temporary Plane
  temporary_plane = create_plane('temporary_plane_deleteme')

  select_all_items('mesh')

  lx.eval('uv.toEPS')
  
  lx.eval('select.item {%s} replace' % temporary_plane)

  lx.eval('item.delete')


# Make line numbers more readable
if __name__ == '__main__':
   try:
      main()
   except:
      lx.out(traceback.format_exc())